Java Coding Best Practice 


1. Use Proper Naming Conventions.
2. Class Members must be accessed privately.
3. Use Underscores in lengthy Numeric Literals.
     (e.g. int num = 58_356_823;)
4. Never leave a Catch Blocks empty.
5. Use StringBuilder or StringBuffer for String Concatenation.
6. Avoid Redundant Initializations.

