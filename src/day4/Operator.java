package src.day4;


public class Operator {

    public static void main(String[] args) {

        System.out.println("*******Arithmetic Operators******");
        // declare variables
        int a = 12, b = 5;

        // addition operator
        System.out.println("a + b = " + (a + b));

        // subtraction operator
        System.out.println("a - b = " + (a - b));

        // multiplication operator
        System.out.println("a * b = " + (a * b));

        // division operator
        System.out.println("a / b = " + (a / b));

        // modulo operator
        System.out.println("a % b = " + (a % b));

        // ***************************************//

        System.out.println("*****Assignment Operator *******");
        // create variables
        int c = 4;
        int var;

        // assign value using =
        var = c;
        System.out.println("var using =: " + var);

        // assign value using =+
        var += c; // var = var+ c = 4+4 = 8
        System.out.println("var using +=: " + var);

        // assign value using =*
        var *= c;
        System.out.println("var using *=: " + var);

        //***********************************//

        System.out.println("*****Relational Operator*****");
        // create variables
        int d = 7, e = 11;

        // value of a and b
        System.out.println("d is " + d + " and e is " + e);

        // == operator
        System.out.println(d == e);  // false

        // != operator
        System.out.println(d != e);  // true

        // > operator
        System.out.println(d > e);  // false

        // < operator
        System.out.println(d < e);  // true

        // >= operator
        System.out.println(d >= e);  // false

        // <= operator
        System.out.println(d <= e);  // true

        //***************************************//

        System.out.println("*******Logical Operator********");

        // && operator
        System.out.println((5 > 3) && (8 > 5));  // true
        System.out.println((5 > 3) && (8 < 5));  // false

        // || operator
        System.out.println((5 < 3) || (8 > 5));  // true
        System.out.println((5 > 3) || (8 < 5));  // true
        System.out.println((5 < 3) || (8 < 5));  // false

        // ! operator
        System.out.println(!(5 == 3));  // true
        System.out.println(!(5 > 3));  // false
    }
}
