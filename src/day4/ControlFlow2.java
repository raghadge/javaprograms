package src.day4;


public class ControlFlow2 {
    public static void main(String[] args) {
        //
        System.out.println("******** FOR LOOP *******");
        int n = 5;
        // for loop
        for (int i = 1; i <= n; ++i) {
            System.out.println(i);
        }

        //********************//

        System.out.println("\n ************ FOR EACH ***********");

        // create an array
        int[] numbers = {3, 7, 5, -5};

        // iterating through the array
        for (int number: numbers) {
            System.out.println(number);
        }

        //**********************//

        System.out.println("\n ******** WHILE LOOP **********");
        // declare variables
        int i = 1, num = 5;

        // while loop from 1 to 5
        while(i <= num) {
            System.out.println(i);
            i++;
        }

        //**********************//
        System.out.println("\n ******** DO-WHILE LOOP **********");
        // do...while loop from 1 to 5
        i=1;

        do {
            System.out.println(i);
            i++;
        } while(i <= num);

    }
}
