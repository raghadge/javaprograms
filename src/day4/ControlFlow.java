package src.day4;


public class ControlFlow {
    public static void main(String[] args) {

        System.out.println("\n ******* IF-ELSE ******");

        int number = 10;
        // checks if number is greater than 0
        if (number > 0) {
            System.out.println("The number is positive.");
        }
        // execute this block
        // if number is not greater than 0
        else {
            System.out.println("The number is not positive.");
        }

        //**********************************//

        System.out.println("\n ******* IF...ELSE...IF ********");

        int number1 = 0;
        // checks if number is greater than 0
        if (number1 > 0) {
            System.out.println("The number is positive.");
        }
        // checks if number is less than 0
        else if (number1 < 0) {
            System.out.println("The number is negative.");
        }
        // if both condition is false
        else {
            System.out.println("The number is 0.");
        }

        //**********************************//

        System.out.println("\n ******** SWITCH STATEMENT *******");

        int expression = 7;  // we can take user input

        // switch statement to check size
        switch (expression) {
                case 1 :
                    System.out.println("Monday");
                    break;
                case 2 :
                    System.out.println("Tuesday");
                    break;
                case 3 :
                    System.out.println("Wednesday");
                    break;
                case 4 :
                    System.out.println("Thursday");
                    break;
                case 5:
                    System.out.println("Friday");
                    break;
                case 6 :
                    System.out.println("Saturday");
                    break;
                default :
                    System.out.println("Sunday");
            }
    }
}
