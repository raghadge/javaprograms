package src.day4;

import java.util.Scanner;


public class UserInput {
    public static void main(String[] args)
    {
        Scanner sc= new Scanner(System.in); //System.in is a standard input stream

        // Input integer
        System.out.print("Enter Integer num : ");
        int num1= sc.nextInt();              //reads string
        System.out.print("You have entered: "+num1);

        //Input Floating Number
        System.out.print("\n Enter a floating num: ");
        float num2= sc.nextFloat();              //reads string
        System.out.print("You have entered: "+num2);

        //Input String
        System.out.print("\n Enter a string: ");
        String str= sc.next();              //reads string
        System.out.print("You have entered: "+str);

    }
}